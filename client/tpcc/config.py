import json
import os
import sys

from . import helpers
from .echo import echo
from .exceptions import ClientError


class Config(object):
    def __init__(self, path, template={}):
        self.path = path
        self.template = template
        self._init()

    def _init(self):
        if not os.path.exists(self.path):
            self._create_new()

        self._load()

    def _create_new(self):
        self._save(self.template)

    def _load(self):
        self.data = helpers.load_json(self.path)

    def _save(self, data):
        with open(self.path, 'w') as f:
            json.dump(data, f, indent=4, sort_keys=True)
        self.data = data

    def show(self):
        echo.echo("Config [{}]:".format(self.path))
        echo.write(json.dumps(self.data, indent=4, sort_keys=True))

    def patch(self, updates):
        self.data.update(updates)
        self._save(self.data)

    def _check_exists(self, path):
        if path not in self.data:
            raise ClientError("Config attribute '{}' not found".format(path))

    def get(self, path):
        self._check_exists(path)
        return self.data[path]

    def set(self, path, value):
        self._check_exists(path)
        self.data[path] = value
        self._save(self.data)
