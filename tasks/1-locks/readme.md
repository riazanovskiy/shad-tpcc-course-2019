## Блокировки

**Задача** | **Баллы** 
--- | ---
`mutex` | 3
`queue-spinlock` | 3
`spinlock` | 2
`toyalloc` | 4
`tricky` | 2
`try-lock` | 2

---

Дедлайн: 6 марта, 18:00.

