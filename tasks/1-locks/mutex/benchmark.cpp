#include <benchmark/benchmark.h>

#include "mutex.hpp"

#include <mutex>

std::mutex std_mutex;
solutions::Mutex mutex;
volatile int count = 0;

// Uncontended

static void BM_StdMutexNoContention(benchmark::State& state) {
  count = 0;
  for (auto _ : state) {
    std::lock_guard<std::mutex> lock(std_mutex);
    ++count;
  }
}

BENCHMARK(BM_StdMutexNoContention);

static void BM_SolutionMutexNoContention(benchmark::State& state) {
  count = 0;
  for (auto _ : state) {
    mutex.Lock();
    ++count;
    mutex.Unlock();
  }
}

BENCHMARK(BM_SolutionMutexNoContention);

// Contended

static void BM_StdMutexContended(benchmark::State& state) {
  if (state.thread_index == 0) {
    count = 0;
  }
  for (auto _ : state) {
    std::lock_guard<std::mutex> lock(std_mutex);
    ++count;
  }
}
BENCHMARK(BM_StdMutexContended)->Threads(2)->Threads(4)->Threads(8)->UseRealTime();

static void BM_SolutionMutexContended(benchmark::State& state) {
  if (state.thread_index == 0) {
    count = 0;
  }
  for (auto _ : state) {
    mutex.Lock();
    ++count;
    mutex.Unlock();
  }
}
BENCHMARK(BM_SolutionMutexContended)->Threads(2)->Threads(4)->Threads(8)->UseRealTime();

BENCHMARK_MAIN();
