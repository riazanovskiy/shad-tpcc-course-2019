#pragma once

#include <cstdint>

// x86-64 only!

// Atomically stores 'value' to memory location 'var'
extern "C" void AtomicStore(volatile std::int64_t* var, std::int64_t value);

// Atomically 1) reads current value from memory location 'var'
// and 2) stores 'value' to 'var'
extern "C" int AtomicExchange(volatile std::int64_t* var, std::int64_t value);
